import ForgeUI, { render, Fragment, Text, IssuePanel, Button, Form, TextField, useState, useEffect } from '@forge/ui';
import { storage } from '@forge/api';

const App = () => {
  const [storedText, setStoredText] = useState("");
  useEffect( async () => {
      const textInput = await storage.get('user-input')
      console.log(textInput)
      setStoredText(textInput)
  }, [storedText]);

  console.log("I am Inevitable...")

  const saveData = async (data) => {
      console.log(data)
      storage.set('user-input', data["user-input"]);
      setStoredText(data["user-input"])
  }

  return (
    <Fragment>
      <Form onSubmit={saveData} >
         <TextField name="user-input" label="Your message" defaultValue = {storedText}/>
      </Form>
    </Fragment>
  );
};

export const run = render(
  <IssuePanel>
    <App />
  </IssuePanel>
);
